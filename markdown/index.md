---
title: "@ottowao"
---

#### ottowao.com | the pseudonymous page for Patrick Kelley — software /devops engineer, open source enthusiast and hobbiest photographer.

### Games

[Cube Runner³](https://ottowao.itch.io/cube-runner)  
Remake of the classic flash game Cubefield!

[Freckles and Constellations](https://aurora-productions.itch.io/freckles-and-constellations)  
A point and click puzzle game made in Unity3D

### Creative

I take the odd photograph here, there and everywhere.  
You can see them here ~ [Unsplash](https://unsplash.com/@ptrkwilliam/)

### Get in touch

[Email](mailto:pat@ottowao.com)  
[GitHub](https://github.com/ottowao)  
[GitLab](https://gitlab.com/ottowao)  

